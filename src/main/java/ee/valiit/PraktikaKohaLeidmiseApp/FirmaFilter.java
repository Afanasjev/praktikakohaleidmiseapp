package ee.valiit.PraktikaKohaLeidmiseApp;

public class FirmaFilter {

    private int id;
    private String kasutajaid;
    private String firmanimi;
    private String kontaktisikunimi;
    private String telefon;
    private String email;
    private String linkedinkontakt;
    private String kommentaar;
    private String kuupaev;

    public FirmaFilter() {
    }

    public FirmaFilter(String kasutajaid, String firmanimi, String kontaktisikunimi, String telefon, String email, String linkedinkontakt, String kommentaar, String kuupaev) {
        this.kasutajaid = kasutajaid;
        this.firmanimi = firmanimi;
        this.kontaktisikunimi = kontaktisikunimi;
        this.telefon = telefon;
        this.email = email;
        this.linkedinkontakt = linkedinkontakt;
        this.kommentaar = kommentaar;
        this.kuupaev = kuupaev;
    }

    public int getId() {
        return id;
    }

    public String getKasutajaid() {
        return Security.xssFix(kasutajaid);
    }

    public String getFirmanimi() {
        return Security.xssFix(firmanimi);
    }

    public String getKontaktisikunimi() {
        return Security.xssFix(kontaktisikunimi);
    }

    public String getTelefon() {
        return Security.xssFix(telefon);
    }

    public String getEmail() {
        return Security.xssFix(email);
    }

    public String getLinkedinkontakt() {
        return Security.xssFix(linkedinkontakt);
    }

    public String getKommentaar() {
        return Security.xssFix(kommentaar);
    }

    public String getKuupaev() {
        return Security.xssFix(kuupaev);
    }

}

